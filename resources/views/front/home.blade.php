@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col" style="color: green;font-size: 66px">
            <public-message :home-message="{{ $message }}"></public-message>
        </div>
    </div>
</div>
@endsection
