<?php
/**
 * PHP Version 7.2
 *
 * @category Model
 * @package  App
 * @author   Thiago Mallon <thiagomallon@gmail.com>
 * @license  MIT https://opensource.org/licenses/MIT
 * @link     https://www.linkedin.com/in/thiago-mallon/
 */

/**
 * File namespace
 */
namespace App;

use Illuminate\Database\Eloquent\Model;


class DisplayNumber extends Model
{
    protected $table= 'display_numbers';

    protected $fillable=['message_id'];

    public function message(): object
    {
        return $this->belongsTo('App\HomeMessage','message_id');
    }
}
